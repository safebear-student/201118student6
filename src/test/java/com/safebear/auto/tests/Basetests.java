package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by CCA_Student on 21/11/2018.
 */
public abstract class Basetests {
    WebDriver driver;
    protected LoginPage loginPage;
    protected ToolsPage toolsPage;

    @BeforeTest
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage (driver);
        toolsPage = new ToolsPage(driver) ;
    }

    @AfterTest
       public void tearDown() {
        driver.quit();

    }
}

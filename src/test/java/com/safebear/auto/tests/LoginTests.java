package com.safebear.auto.tests;

import com.safebear.auto.tests.Basetests;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by CCA_Student on 21/11/2018.
 */
public class LoginTests extends Basetests{
    @Test
    public void loginTest(){
    //Step 1 open the web app is browser
driver.get(Utils.getUrl());
//step 1a assert we're on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page", "The login page didn't open");
//step 2 login
    loginPage.login("tester", "letmein");
    //streps 2a assert we arre on tools page
    Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page","meh");

}
}
